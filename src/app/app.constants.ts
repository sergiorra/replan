export class AppConstants {

  public static urlAPI = 'http://replan-api.herokuapp.com/replan/projects/';

  public static LOW_FEATURE_EFFORT = 5;
  public static HIGH_FEATURE_EFFORT = 30;

  public static LOW_RESOURCE_AVAILABILITY = 0;
  public static MEDIUM_RESOURCE_AVAILABILITY = 25;
  public static HIGH_RESOURCE_AVAILABILITY = 50;

}
